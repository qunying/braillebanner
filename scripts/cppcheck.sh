#!/bin/sh
cppcheck --enable=all --inline-suppr --suppress=missingIncludeSystem  \
	--suppress=unusedFunction --check-level=exhaustive $*
