⠸⣿⣿⡿⡿⢷⡄⣤⣤⣤⣀⠀⠀⣴⣶⡀⠀⢠⣤⡄⢠⣤⣤⠀⠀⢠⣤⣤⠀⠀⢠⣤⣤⣤⣤⢠⣾⡿⢿⣿⣧⢿⣿⣿⢿⠿⣦⠀⢠⣶⣆⠀⠀⣤⣤⠀⠀⣤⣤⠀⣤⣤⠀⠀⣤⣤⠀⣤⣤⣤⣤⡄⣤⣤⣤⣀⠀
⠀⣿⣿⢁⣀⡞⠁⢿⡏⠋⣿⡆⣼⡿⠙⣷⡀⠀⣿⡇⠀⣿⡇⠀⠀⠀⣿⡇⠀⠀⠸⣿⣀⣁⡙⠈⠻⣷⣌⠀⠏⢸⣿⡏⣀⣰⠋⢠⣿⠏⢻⣆⠀⣿⣿⣷⠀⣿⠇⠀⣿⣿⣷⠀⣿⠇⠀⢿⣇⣈⣈⠃⢿⡏⠋⣿⡆
⠀⢹⣿⠋⠙⣿⡆⢸⡧⣴⠛⠐⣿⠷⠶⣾⡇⠀⢿⡇⠀⢸⡇⠀⣀⡀⢸⡇⠀⣀⡀⢿⡀⠘⠀⠀⡄⠸⠛⣷⡄⠈⣿⡟⠉⢻⣷⢺⡿⠶⢶⣿⠀⣹⡇⠻⣧⣿⠀⠀⣹⡇⠻⣧⣿⠀⠀⠸⣇⠀⠃⠀⢸⡧⣴⠛⠀
⠀⢸⡿⢲⢻⠛⠁⠸⠃⠈⠗⠀⠏⠀⠀⠀⠇⠀⠸⠀⠀⠈⠏⠉⠹⠁⠈⠏⠉⠹⠁⠸⠟⠙⠁⠀⠙⢶⡾⠛⠁⠀⣿⠗⡞⡟⠋⠸⠁⠀⠀⠸⠀⠀⠇⠀⠀⠋⠀⠀⠀⠇⠀⠀⠋⠀⠀⠀⠿⠋⠋⠀⠸⠃⠈⠗⠀
⠀⠀⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠁⠀⠀⠀⠀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
This is a toy program trying to render text with braille symbols like
the banner program.

It could run from command line and render the output to stdout or
run without arguments and play with various settings in a ncurses based
TUI.

Just for fun!

PS.
The logo in this README was generated with the following command:
braillesbanner -f "QTGhoulFace 15" BrailleSBanner  -r 0
