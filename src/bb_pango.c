/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <pango/pango-layout.h>
#include <pango/pangocairo.h>

#include "bb_pango.h"
#include "bb_braille_draw.h"

static PangoContext * bb_pango_create_context(PangoFontMap *fn_map,
                                              bool         antialias);

bool
bb_pango_render(PangoFontMap *map,
                const char   *fn_str,
                const char   *msg,
                uint16_t     rotation,
                uint8_t      threshold,
                bool         antialias,
                bool         inverse,
                uint8_t      shift_down,
                uint8_t      shift_right,
                const char   **err_msg,
                uint8_t      **out_dat,
                int          *out_width,
                int          *out_height)
{
    bool                 ret      = false;
    PangoContext         *context = NULL;
    PangoLayout          *layout  = NULL;
    cairo_surface_t      *surface = NULL;
    cairo_t              *cr      = NULL;
    PangoFontDescription *fn_desc;
    const uint8_t        *img_dat;
    int                  width, height;
    double               x_offset = shift_right;
    double               y_offset = shift_down;

    context = bb_pango_create_context(map, antialias);
    if (context == NULL) {
        if (err_msg) *err_msg = "failed to create pango context";
        return false;
    }
    layout = pango_layout_new(context);
    if (layout == NULL) {
        if (err_msg) *err_msg = "failed to create pango layout";
        goto exit;
    }

    fn_desc = pango_font_description_from_string(fn_str);
    if (fn_desc == NULL) {
        if (err_msg) *err_msg = "failed to create font descritption.";
        goto exit;
    }
    pango_layout_set_font_description(layout, fn_desc);
    pango_font_description_free(fn_desc);

    pango_layout_set_auto_dir(layout, TRUE);
    pango_layout_set_markup(layout, msg, -1);
    pango_layout_get_pixel_size(layout, &width, &height);

    // rotation
    if (rotation == 90 || rotation == 270) {
        int tmp;
        tmp    = width;
        width  = height;
        height = tmp;

        if (rotation == 90) {
            x_offset = shift_down;
            y_offset = -width - shift_right;
        } else {
            x_offset = -height - shift_down;
            y_offset = shift_right;
        }
    } else if (rotation == 180) {
        x_offset = -width - shift_right;
        y_offset = -height - shift_down;
    }
    width  += shift_right * 2;
    height += shift_down * 2;
    // create a surface
    surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24,
                                         width, height);
    cr = cairo_create(surface);
    cairo_set_source_rgb(cr, 1, 1, 1);
    pango_cairo_update_context(cr, context);

    if (rotation) {
        cairo_rotate(cr, rotation * G_PI / 180.);
    }
    pango_cairo_update_layout(cr, layout);
    if (x_offset || y_offset) {
        cairo_move_to(cr, x_offset, y_offset);
    }
    pango_cairo_show_layout(cr, layout);

    cairo_destroy(cr);
    img_dat = cairo_image_surface_get_data(surface);

    bb_braille_draw(img_dat, width, height, 32, width * 4,
                    threshold, inverse,
                    out_dat, out_width, out_height);
    cairo_surface_destroy(surface);
    ret = true;
exit:
    g_object_unref(layout);
    g_object_unref(context);

    return ret;
} /* bb_pango_render */

static PangoContext *
bb_pango_create_context(PangoFontMap *fn_map, bool antialias)
{
    cairo_font_options_t *font_options;

    PangoContext *context = pango_font_map_create_context(fn_map);
    if (context == NULL) {
        return NULL;
    }

    font_options = cairo_font_options_create();
    if (font_options == NULL) {
        g_object_unref(context);
        return NULL;
    }

    cairo_font_options_set_hint_style(font_options, CAIRO_HINT_STYLE_FULL);
    if (!antialias) {
        cairo_font_options_set_antialias(font_options, CAIRO_ANTIALIAS_NONE);
        pango_cairo_context_set_font_options(context, font_options);
    }
    cairo_font_options_destroy(font_options);
    return context;
} /* bb_pango_create_context */

/* bb_pango_render */
