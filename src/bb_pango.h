/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#ifndef BB_PANGO_H
#define BB_PANGO_H

#include <stdbool.h>
#include <pango/pango-fontmap.h>
#include "bb_conf.h"

bool bb_pango_render(PangoFontMap *map,
                     const char   *fn_str,
                     const char   *msg,
                     uint16_t     rotation,
                     uint8_t      threshold,
                     bool         antialias,
                     bool         inverse,
                     uint8_t      shift_down,
                     uint8_t      shift_right,
                     const char   **err_msg,
                     uint8_t      **out_dat,
                     int          *out_width,
                     int          *out_height);
#endif // ifndef BB_PANGO_H

/* vim: set ts=8 sts=4 sw=4 smarttab expandtab spell */
