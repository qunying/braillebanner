/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <argp.h>
#include <error.h>
#include <stdlib.h>
#include <string.h>

#include "bb_conf.h"
#include "bb_parse_argv.h"
#include "src/bb_conf.h.in"

const char *argp_program_version = BB_VERSION;

static char argp_prog_doc[] =
    BB_VER_COPYRIGHT_STR "\n"
    "Released under GPLv3 or latter."
    "\v"
    "Font description follow "
    "https://docs.gtk.org/Pango/type_func.FontDescription.from_string.html\n"
    "\nMessage could be in the format of Pango markup "
    "https://docs.gtk.org/Pango/pango_markup.html\n"
    "\nThis program required terminal to support utf-8 and have the "
    "braille font installed.";

static char args_doc[] = "[message]";

static struct argp_option options[] = {
    {.name  = "font",
     .key   = 'f',
     .arg   = "FONT-DESC",
     .flags = 0,
     .doc   = "Font description, as used in pango.",
     .group = 0},
    {.name  = "rotation",
     .key   = 'r',
     .arg   = "NUM",
     .flags = 0,
     .doc   = "Bannner display rotaion angle (0,90,180,270), default 90 "
              "degree for vertical dispaly.",
     .group = 0},
    {.name  = "antialias",
     .key   = 'a',
     .arg   = NULL,
     .flags = 0,
     .doc   = "Turn on antialias when rendering with pango.",
     .group = 0},
    {.name  = "threshold",
     .key   = 't',
     .arg   = "NUM",
     .flags = 0,
     .doc   = "Set threshold value to consider as valid dot in image when "
              "antialias is on (30~255, default 96.)",
     .group = 0},
    {.name  = "inverse",
     .key   = 'i',
     .arg   = NULL,
     .flags = 0,
     .doc   = "Inverse the resulting output.",
     .group = 0},
    {.name  = "shift-down",
     .key   = 'd',
     .arg   = "NUM",
     .flags = 0,
     .doc   = "Shift result down for the given pixel (0~4)",
     .group = 1},
    {.name  = "shift-right",
     .key   = 's',
     .arg   = "NUM",
     .flags = 0,
     .doc   = "Shift result right for the given pixel (0~4)",
     .group = 1},
    {.name  = "logo",
     .key   = 'l',
     .arg   = 0,
     .flags = 0,
     .doc   = "Display this program's logo.",
     .group = 2},
    {NULL},
};
static error_t parse_opt(int key, char *arg, struct argp_state *state);

static struct argp argp = {
    .options     = options,
    .parser      = parse_opt,
    .args_doc    = args_doc,
    .doc         = argp_prog_doc,
    .children    = NULL,
    .help_filter = NULL,
    .argp_domain = NULL,
};

void
bb_parse_argv(int argc, char *argv[], bb_conf_t *conf)
{
    const char *env = getenv("ARGP_HELP_FMT");
    // get rid of the "Mandatory or optional arguments ..." line.
    if (!env) {
        setenv("ARGP_HELP_FMT", "no-dup-args-note", 0);
    }
    argp_parse(&argp, argc, argv, 0, 0, conf);
} /* bb_parse_argv */

static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
    bb_conf_t *conf   = state->input;
    char      *endptr = NULL;
    long      num;

    switch (key) {
    case 'f': conf->font = arg; break;
    case 'r':
        num = strtol(arg, &endptr, 10);
        // currently, only 0,90,180,270
        if (num != 0 && num != 90 && num != 180 && num != 270) {
            error(1, 0, "invalid rotation value.");
        }
        conf->cur_rotation = num;
        break;
    case 'a':
        conf->cur_antialias = true;
        break;
    case 't':
        num = strtol(arg, &endptr, 10);
        if ((num < BB_MIN_THRESHOLD || num > 255) || (endptr && endptr[0])) {
            error(1, 0, "invalid threshold value.");
        }
        conf->cur_threshold = num;
        break;
    case 'i':
        conf->cur_inverse = true;
        break;
    case 'l':
        conf->show_logo = true;
        return 0;
        break;
    case 'd':
    case 's':
        num = strtol(arg, &endptr, 10);
        if (num < 0 || num > 4 || (endptr && endptr[0])) {
            error(1, 0, "invalid shift value.");
        }
        if (key == 'd')
            conf->cur_shift_down = num;
        else
            conf->cur_shift_right = num;
        break;
    case ARGP_KEY_ARG:
        conf->arg = arg;
        if (state->argv[state->next] != NULL) {
            error(1, 0, "too many arguments");
        }
        state->next = state->argc;
        break;
    default: return ARGP_ERR_UNKNOWN;
    } /* swith */

    return 0;
} /* parse_opt */
/* vim: set ts=8 sts=3 sw=3 smarttab expandtab spell */
