/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#ifndef BB_DISPLAY_DEFAULT_BANNER_H
#define BB_DISPLAY_DEFAULT_BANNER_H

void bb_display_default_banner(const bb_conf_t *conf, const char *msg);

#endif // ifndef BB_DISPLAY_DEFAULT_BANNER_H
/* vim: set ts=8 sts=3 sw=3 smarttab expandtab spell */
