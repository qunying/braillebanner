/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <pango/pangocairo.h>
#include "bb_pango.h"
#include "bb_conf.h"
#include "bb_display_default_banner.h"

void
bb_display_default_banner(const bb_conf_t *conf, const char *msg)
{
    PangoFontMap *font_map;
    const char   *err_msg = NULL;


    // found the width and height of the rendered  result
    font_map = pango_cairo_font_map_new();
    if (!bb_pango_render(font_map, conf->font, msg,
                         conf->cur_rotation,
                         conf->cur_threshold,
                         conf->cur_antialias,
                         conf->cur_inverse,
                         conf->cur_shift_down,
                         conf->cur_shift_right,
                         &err_msg,
                         NULL, NULL, NULL)) {
        if (err_msg)
            fprintf(stderr, "error: %s\n", err_msg);
        else
            fputs("failed to render the message.", stderr);
    }
    g_object_unref(font_map);

} /* bb_display_default_bannder */

/* vim: set ts=8 sts=3 sw=3 smarttab expandtab spell */
