/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#ifndef BB_TUI_H
#define BB_TUI_H

#include "bb_conf.h"

void bb_tui(bb_conf_t *conf);
#endif // ifndef BB_TUI_H

/* vim: set ts=8 sts=4 sw=4 smarttab expandtab spell */
