/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#ifndef BB_BRAIL_DRAW_H
#define BB_BRAIL_DRAW_H
#include <stdbool.h>
#include <stdint.h>

// input image is always from left to right with
bool bb_braille_draw(const uint8_t *img_dat,
                     int           width,
                     int           height,
                     int           bpp,
                     int           pitch,
                     uint8_t       threshold,
                     bool          inverse,
                     uint8_t       **out_dat,
                     int           *o_width,
                     int           *o_height);
#endif // ifndef BB_BRAIL_DRAW_H

/* vim: set ts=8 sts=3 sw=3 smarttab expandtab spell */
