/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <curses.h>
#include <ncursesw/ncurses.h>
#include <ncursesw/panel.h>
#include <stdbool.h>

#include <pango/pango-font.h>
#include <pango/pangocairo.h>

#include "bb_conf.h"
#include "bb_tui.h"
#include "bb_pango.h"
#include "bb_braille_draw.h"
#include "src/bb_conf.h.in"

/*
   -----------------------------------------------
   | message box                                 |
   |---------------------------------------------|
   | font selection |  Sample display of message |
   | font family    |                            |
   | font size      |                            |
   | threshold      |
   | inverse        |                            |
   |                |                            |
   |                |                            |
   -----------------------------------------------
   | status                                      |
   -----------------------------------------------
*/

#define quit_msg  "Press CTRL+Q to quit."

#define CTRL_KEY(x) ((x) & 0x1F)

#define STATUS_COLOR       1
#define SELECTED_COLOR     2
#define UNSELECTED_COLOR   3
#define SEPARATION_COLOR   4

#define FONT_WIN_WIDTH      30

#define FONT_NAME_OFFSET    1
#define FONT_STYLE_OFFSET   3
#define FONT_SZ_OFFSET      4
#define ROTATION_OFFSET     6
#define ANTIALIAS_OFFSET    7
#define THRESHOLD_OFFSET    8
#define SHIFT_DOWN_OFFSET   9
#define SHIFT_RIGHT_OFFSET  10
#define INVERSE_OFFSET      11

typedef enum {
    MSG_WIN_ID,
    OPTIONS_WIN_ID,
    DISPLAY_WIN_ID,
    STATUS_WIN_ID,
    RESIZE_WIN_ID,
    MAX_WIN_ID,
} bb_win_id_t;

typedef enum {
    BB_ACTIVE_MIN,
    BB_ACTIVE_MSG = BB_ACTIVE_MIN,
    BB_ACTIVE_FONT_NAME,
    BB_ACTIVE_FONT_STYLE,
    BB_ACTIVE_FONT_SZ,
    BB_ACTIVE_ROTATION,
    BB_ACTIVE_ANTIALIAS,
    BB_ACTIVE_THRESHOLD,
    BB_ACTIVE_SHIFT_DOWN,
    BB_ACTIVE_SHIFT_RIGHT,
    BB_ACTIVE_INVERSE,
    BB_ACTIVE_MAX = BB_ACTIVE_INVERSE,
} bb_tui_field_t;

typedef struct bb_f_name_st {
    const char *name;
    int        len;
} bb_fn_name_t;

typedef struct bb_fonts_st {
    /* Font related fields */
    PangoFontMap    *map;
    PangoFontFamily **families;
    int             n_families;
    bb_fn_name_t    *names;

    const char      **style;


    int             style_idx;
    int             cur_style_idx;
    int             sz;
    int             cur_sz;
    int             font_idx;
    int             cur_font_idx;

    uint16_t        rotation;
    uint8_t         threshold;
    uint8_t         shift_down;
    uint8_t         shift_right;
    bool            antialias;
    bool            inverse; // inverse the output image.
} bb_fonts_t;

typedef struct bb_tui_st {
    bb_conf_t      *conf;
    PANEL          *panel[MAX_WIN_ID];
    int            width;
    int            height;
    int            cur_width;
    int            cur_height;
    char           msg[80];
    bb_tui_field_t active_field;
    bb_tui_field_t cur_active_field;
    bb_fonts_t     fonts;
    short          sel_pair;
    short          unsel_pair;
    short          status_pair;
    short          sep_pair;
    uint8_t        msg_len;
    uint8_t        cur_msg_len;
    uint8_t        shift_down;
    uint8_t        shift_right;
    bool           screen_ok;
    bool           screen_too_small;
    bool           resize;
    const char     *err_msg;
} bb_tui_t;

static void exit_tui(void);
static void init_screen(bb_tui_t *tui);
static void init_color_pair(bb_tui_t *tui);
static bool check_terminal_size(bb_tui_t *tui);
static bool refresh_tui(bb_tui_t *tui);
static bool create_new_tui(bb_tui_t *tui);
static bool update_tui(bb_tui_t *tui);
static void display_status_win(bb_tui_t *tui);
static void display_msg(bb_tui_t *tui);
static bool display_options(bb_tui_t *tui);
static bool load_font_maps(bb_tui_t *tui);
static void tui_ctrl_option_update(bb_tui_t *tui, wint_t key);
static bool render_msg(bb_tui_t *tui);
static void show_logo(bb_tui_t *tui);

static void increment_active_field_value(bb_tui_t *tui);
static void decrement_active_field_value(bb_tui_t *tui);

void
bb_tui(bb_conf_t *conf)
{
#define NUM_OF_STYLES 4
    const char *style[NUM_OF_STYLES] = {
        "Normal", "Roman", "Oblique", "Italic",
    };
    bb_tui_t   tui = {
        .conf             = conf,
        .width            = 0,
        .height           = 0,
        .cur_width        = 0,
        .cur_height       = 0,
        .active_field     = BB_ACTIVE_MSG,
        .cur_active_field = BB_ACTIVE_MSG,
        .fonts            = {
            .style         = style,
            .style_idx     = 0,
            .cur_style_idx = 0,
            .sz            = 25,
            .cur_sz        = 25,
            .font_idx      = 0,
            .cur_font_idx  = 0,
            .threshold     = conf->cur_threshold,
            .antialias     = conf->cur_antialias,
        },
        .msg_len          = 0,
        .cur_msg_len      = 0,
        .screen_ok        = false,
        .screen_too_small = false,
        .err_msg          = NULL,
    };
    bool       got_escape_key      = false;
    bool       got_open_sq_bracket = false;
    bool       signal_stopped      = false;

    init_screen(&tui);
    if (!check_terminal_size(&tui))
        tui.screen_too_small = true;

    if (!refresh_tui(&tui))
        goto exit;

    while (true) {
        int key;
        if (signal_stopped) {
            refresh();
            if (tui.cur_active_field == BB_ACTIVE_MSG) {
                display_msg(&tui);
                update_panels();
                doupdate();
            }
            signal_stopped = false;
        }
        key = getch();
        switch (key) {
        case CTRL_KEY('q'): goto exit;
        case CTRL_KEY('z'):
            endwin();
            signal_stopped = true;
            raise(SIGSTOP);
            break;
        case KEY_RESIZE:
            clear();
            if (!check_terminal_size(&tui)) {
                tui.screen_too_small = true;
                tui.screen_ok        = false;
            } else {
                tui.screen_too_small = false;
            }
            tui.resize = true;
            if (!refresh_tui(&tui)) goto exit;
            break;
        case '\t':
            ++tui.cur_active_field;
            if (tui.cur_active_field > BB_ACTIVE_MAX)
                tui.cur_active_field = BB_ACTIVE_MIN;
            if (!refresh_tui(&tui)) goto exit;
            break;
        case '\x1b':
            got_escape_key = true;
            break; // skip escape key
        case KEY_MOUSE: {
            MEVENT event = {0};
            if (!tui.screen_ok) break;
            if (getmouse(&event) == OK) {
                if (event.bstate & BUTTON1_CLICKED) {
                    if (event.y == 0) { // msg window
                        tui.cur_active_field = BB_ACTIVE_MSG;
                    } else if (event.x < FONT_WIN_WIDTH) {
                        if (event.y == 1 + FONT_NAME_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_FONT_NAME;
                        } else if (event.y == 1 + FONT_STYLE_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_FONT_STYLE;
                        } else if (event.y == 1 + FONT_SZ_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_FONT_SZ;
                        } else if (event.y == 1 + ROTATION_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_ROTATION;
                        } else if (event.y == 1 + ANTIALIAS_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_ANTIALIAS;
                        } else if (event.y == 1 + THRESHOLD_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_THRESHOLD;
                        } else if (event.y == 1 + SHIFT_DOWN_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_SHIFT_DOWN;
                        } else if (event.y == 1 + SHIFT_RIGHT_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_SHIFT_RIGHT;
                        } else if (event.y == 1 + INVERSE_OFFSET) {
                            tui.cur_active_field = BB_ACTIVE_INVERSE;
                        }  // if
                    } // if
                    if (!refresh_tui(&tui)) goto exit;
                } else if (event.bstate & BUTTON3_CLICKED) {
                    increment_active_field_value(&tui);
                    display_status_win(&tui);
                    display_options(&tui);
                    if (!refresh_tui(&tui)) goto exit;
                } else if (event.bstate & BUTTON2_CLICKED) {
                    decrement_active_field_value(&tui);
                    display_status_win(&tui);
                    display_options(&tui);
                    if (!refresh_tui(&tui)) goto exit;
                }// if
            } // if
            break;
        }
        default:
            if (!tui.screen_ok) break;
            if (got_escape_key) {
                if (!got_open_sq_bracket) {
                    if (key == '[') {
                        got_open_sq_bracket = true;
                        break;
                    }
                } else if (key == 'Z') { // got shift-tab
                    got_escape_key      = false;
                    got_open_sq_bracket = false;
                    --tui.cur_active_field;
                    if (tui.cur_active_field < BB_ACTIVE_MIN)
                        tui.cur_active_field = BB_ACTIVE_MAX;
                    if (!refresh_tui(&tui)) goto exit;
                    break;
                }
                got_escape_key      = false;
                got_open_sq_bracket = false;
            }

            switch (tui.cur_active_field) {
            case BB_ACTIVE_MSG:
                if (key == KEY_BACKSPACE || key == CTRL_KEY('g')
                    || key == 0x7f) {
                    // extremely simplified del action for utf-8, only remove
                    // one code point, regardless is part of surrogate or
                    // grapheme cluster
                    if (tui.cur_msg_len == 0)
                        break;
                    while (true) {
                        if ((uint8_t)tui.msg[tui.cur_msg_len - 1] <= 0x7F) {
                            --tui.cur_msg_len;
                            break;
                        } else if ((tui.msg[tui.cur_msg_len - 1] & 0xC0)
                                   == 0x80) {
                            --tui.cur_msg_len;
                        } else {
                            --tui.cur_msg_len;
                            break;
                        }
                    }
                    tui.msg[tui.cur_msg_len] = '\0';
                } else {
                    if ((key >= KEY_MIN && key != KEY_DC) || key < 0x20)
                        break;
                    if (tui.cur_msg_len + 1 < (int)sizeof(tui.msg)) {
                        tui.msg[tui.cur_msg_len] = key;
                        ++tui.cur_msg_len;
                    }
                }
                display_msg(&tui);
                break;
            default:
                tui_ctrl_option_update(&tui, key);
                break;
            } /* switch */
            if (!render_msg(&tui))
                break;
            update_panels();
            doupdate();
            break;
        } /* switch */
    } /* while */
exit:
    endwin();
    if (tui.err_msg) {
        puts(tui.err_msg);
    }
    if (tui.fonts.families) g_free(tui.fonts.families);
    if (tui.fonts.names) free(tui.fonts.names);
    if (tui.fonts.map) g_object_unref(tui.fonts.map);
} /* bb_tui */

static void
exit_tui(void)
{
    endwin();
} /* exit_tui */

static void
init_screen(bb_tui_t *tui)
{

    use_tioctl(TRUE);
    initscr();
    start_color();
    use_default_colors();
    assume_default_colors(COLOR_WHITE, COLOR_BLACK);
    atexit(exit_tui);
    raw(); // turn off tty buffer and software flow control
    noecho(); // don't echo
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);
    init_color_pair(tui);
    refresh();

    mousemask(ALL_MOUSE_EVENTS, NULL);
} /* init_screen */

static bool
check_terminal_size(bb_tui_t *tui)
{
    int cur_width;
    int cur_height;

    getmaxyx(stdscr, cur_height, cur_width);
    if (cur_width < 80 || cur_height < 25) {
        int    x, y;
        WINDOW *tmp_win = NULL;
        if (tui->panel[RESIZE_WIN_ID] == NULL) {
            tmp_win = newwin(cur_height, cur_width, 0, 0);
            if (tmp_win == NULL) {
                tui->err_msg = "failed to crate resize window";
                return false;
            }
            tui->panel[RESIZE_WIN_ID] = new_panel(tmp_win);
            if (tui->panel[RESIZE_WIN_ID] == NULL) {
                tui->err_msg = "failed to crate resize panel";
                delwin(tmp_win);
                return false;
            }
        } else if (cur_height != tui->cur_height
                   || cur_width != tui->cur_width) {
            WINDOW *owin = panel_window(tui->panel[RESIZE_WIN_ID]);
            tmp_win = newwin(cur_height, cur_width, 0, 0);
            if (tmp_win == NULL) {
                tui->err_msg = "failed to crate resize window";
                return false;
            }
            replace_panel(tui->panel[RESIZE_WIN_ID], tmp_win);
            delwin(owin);
        }
        tmp_win = panel_window(tui->panel[RESIZE_WIN_ID]);
#define prompt "Terminal size too small, needs 80x25."
        x = (cur_width - sizeof(prompt) + 1) / 2;
        y = (cur_height - 2) / 2;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        clear();
        mvwaddstr(tmp_win, y, x, prompt);
        y = getcury(tmp_win) + 1;
        mvwaddstr(tmp_win, y, x, quit_msg);
        top_panel(tui->panel[RESIZE_WIN_ID]);
        for (int i = 0; i < MAX_WIN_ID; ++i) {
            if (i != RESIZE_WIN_ID && tui->panel[i])
                hide_panel(tui->panel[i]);
        }
        update_panels();
        doupdate();
        tui->cur_width  = cur_width;
        tui->cur_height = cur_height;
        return false;
    }
    tui->cur_width  = cur_width;
    tui->cur_height = cur_height;
    return true;
} /* check_terminal_size */

static bool
refresh_tui(bb_tui_t *tui)
{
    if (tui->screen_too_small) return true;
    if (!tui->screen_ok && tui->panel[MSG_WIN_ID] == NULL) {
        // need to create new window
        return create_new_tui(tui);
    }
    return update_tui(tui);
} /* refresh_tui */

static bool
create_new_tui(bb_tui_t *tui)
{
    WINDOW *tmp_win;

    tui->width  = tui->cur_width;
    tui->height = tui->cur_height;

    tmp_win = newwin(1, tui->width, 0, 0);
    if (tmp_win == NULL) {
        tui->err_msg = "failed to create msg window.";
        return false;
    }
    tui->panel[MSG_WIN_ID] = new_panel(tmp_win);
    if (tui->panel[MSG_WIN_ID] == NULL) {
        tui->err_msg = "failed to create msg panel.";
        goto error;
    }

    tmp_win = newwin(tui->height - 2, FONT_WIN_WIDTH, 1, 0);
    if (tmp_win == NULL) {
        tui->err_msg = "failed to create font window.";
        goto error;
    }
    tui->panel[OPTIONS_WIN_ID] = new_panel(tmp_win);
    if (tui->panel[OPTIONS_WIN_ID] == NULL) {
        tui->err_msg = "failed to font panel.";
        goto error;
    }

    tmp_win = newwin(tui->height - 2, tui->width - FONT_WIN_WIDTH, 1,
                     FONT_WIN_WIDTH);
    if (tmp_win == NULL) {
        tui->err_msg = "failed to create display window.";
        goto error;
    }
    tui->panel[DISPLAY_WIN_ID] = new_panel(tmp_win);
    if (tui->panel[DISPLAY_WIN_ID] == NULL) {
        tui->err_msg = "failed to create display panel.";
        goto error;
    }

    tmp_win = newwin(1, tui->width, tui->height - 1, 0);
    if (tmp_win == NULL) {
        tui->err_msg = "failed to create status window.";
        goto error;
    }
    tui->panel[STATUS_WIN_ID] = new_panel(tmp_win);
    if (tui->panel[DISPLAY_WIN_ID] == NULL) {
        tui->err_msg = "failed to create status panel.";
        return false;
    }

    display_options(tui);
    display_status_win(tui);
    display_msg(tui);
    show_logo(tui);
    update_panels();
    doupdate();
    tui->screen_ok = true;
    return true;

error:
    if (tmp_win) delwin(tmp_win);
    for (int i = 0; i < MAX_WIN_ID; ++i) {
        if (tui->panel[i]) {
            tmp_win = panel_window(tui->panel[i]);
            delwin(tmp_win);
            del_panel(tui->panel[i]);
            tui->panel[i] = NULL;
        } else {
            break;
        }
    }
    return false;
} /* create_new_tui */

static bool
update_tui(bb_tui_t *tui)
{
    if (tui->width != tui->cur_width || tui->height != tui->cur_height) {
        // resize is needed for font/display/status window
        WINDOW *old_win;
        WINDOW *tmp_win;

        tui->width  = tui->cur_width;
        tui->height = tui->cur_height;

        tmp_win = newwin(1, tui->cur_width, 0, 0);
        if (tmp_win == NULL) {
            tui->err_msg = "failed to create new msg window";
            return false;
        }
        old_win = panel_window(tui->panel[MSG_WIN_ID]);
        replace_panel(tui->panel[MSG_WIN_ID], tmp_win);
        delwin(old_win);
        display_msg(tui);
        tmp_win = newwin(tui->height - 2, FONT_WIN_WIDTH, 1, 0);
        if (tmp_win == NULL) {
            tui->err_msg = "failed to create new font window";
            return false;
        }
        old_win = panel_window(tui->panel[OPTIONS_WIN_ID]);
        replace_panel(tui->panel[OPTIONS_WIN_ID], tmp_win);
        delwin(old_win);
        if (!display_options(tui)) {
            tui->err_msg = "fail to display font selection";
            return false;
        }

        tmp_win = newwin(tui->height - 2, tui->width - FONT_WIN_WIDTH, 1,
                         FONT_WIN_WIDTH);
        if (tmp_win == NULL) {
            tui->err_msg = "failed to create new display window";
            return false;
        }
        old_win = panel_window(tui->panel[DISPLAY_WIN_ID]);
        replace_panel(tui->panel[DISPLAY_WIN_ID], tmp_win);
        delwin(old_win);

        tmp_win = newwin(1, tui->width, tui->height - 1, 0);
        if (tmp_win == NULL) {
            tui->err_msg = "failed to create new status window";
            return false;
        }
        old_win = panel_window(tui->panel[STATUS_WIN_ID]);
        replace_panel(tui->panel[STATUS_WIN_ID], tmp_win);
        delwin(old_win);
        display_status_win(tui);
    }
    for (int i = 0; i < MAX_WIN_ID; ++i) {
        if (i != RESIZE_WIN_ID) {
            show_panel(tui->panel[i]);
        } else if (tui->panel[i]) {
            hide_panel(tui->panel[i]);
        }
    }
    if (tui->cur_active_field != tui->active_field) {
        display_msg(tui);
        display_options(tui);
        tui->active_field = tui->cur_active_field;
        if (tui->active_field != BB_ACTIVE_MSG) {
            curs_set(0);
        } else {
            curs_set(1);
        }
    }
    render_msg(tui);
    update_panels();
    doupdate();
    tui->screen_ok = true;
    return true;
} /* update_tui */

static void
display_status_win(bb_tui_t *tui)
{
    WINDOW *win = panel_window(tui->panel[STATUS_WIN_ID]);

    wmove(win, 0, 0);
    wclrtoeol(win);
    wbkgd(win, COLOR_PAIR(STATUS_COLOR));
    waddstr(win, "Ctrl-Q:Quit|Tab:Switch Field|←↕→:Change Value");
    if (tui->fonts.n_families) {
        char buf[80];
        int  n;
        int  width;

        width = getmaxx(win);
        n     = snprintf(buf, sizeof(buf), "Font %d/%d",
                         tui->fonts.cur_font_idx + 1, tui->fonts.n_families);

        mvwaddstr(win, 0, width - n, buf);
    }
} /* display_status_win */

static void
display_msg(bb_tui_t *tui)
{
#define BANNER_MSG  "Enter Banner message:"
    WINDOW *win     = panel_window(tui->panel[MSG_WIN_ID]);
    bool   selected = tui->cur_active_field == BB_ACTIVE_MSG;
    short  pair;
    int    width;

    wmove(win, 0, 0);
    pair = selected ? tui->sel_pair : tui->unsel_pair;

    wclrtoeol(win);
    waddstr(win, BANNER_MSG);
    width = getmaxx(win);
    wattrset(win, pair);
    wmove(win, 0, sizeof(BANNER_MSG));
    for (int i = 0; i < width - (int)sizeof(BANNER_MSG) - 1; ++i) {
        waddch(win, ' ');
    }
    wmove(win, 0, sizeof(BANNER_MSG));
    if (tui->cur_msg_len) {
        waddstr(win, tui->msg);
    }
    wattroff(win, pair);
    if (selected)
        top_panel(tui->panel[MSG_WIN_ID]);
} /* display_msg */

static void
init_color_pair(bb_tui_t *tui)
{
    init_pair(STATUS_COLOR, COLOR_RED, COLOR_YELLOW);
    init_pair(SELECTED_COLOR, COLOR_YELLOW, COLOR_RED);
    init_pair(UNSELECTED_COLOR, COLOR_BLACK, COLOR_WHITE);
    init_pair(SEPARATION_COLOR, COLOR_RED, COLOR_BLACK);

    tui->status_pair = COLOR_PAIR(STATUS_COLOR);
    tui->sel_pair    = COLOR_PAIR(SELECTED_COLOR);
    tui->unsel_pair  = COLOR_PAIR(UNSELECTED_COLOR);
    tui->sep_pair    = COLOR_PAIR(SEPARATION_COLOR);
} /* init_color_pair */

static bool
display_options(bb_tui_t *tui)
{
    int          height, width;
    short        pair;
    WINDOW       *win;
    bb_fonts_t   *fonts;
    char         buf_str[40];
    bb_fn_name_t *font_name;
    bb_conf_t    *conf = tui->conf;

    win = panel_window(tui->panel[OPTIONS_WIN_ID]);
    getmaxyx(win, height, width);

    if (!load_font_maps(tui))
        return false;

    fonts = &tui->fonts;
    pair  = (tui->cur_active_field == BB_ACTIVE_FONT_NAME)
            ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, FONT_NAME_OFFSET, 0, "Font: ");
    wclrtoeol(win);
    wattrset(win, pair);
    font_name = &tui->fonts.names[fonts->cur_font_idx];
    if (font_name->len > width - 7) {
        snprintf(buf_str, sizeof(buf_str), "%.*s", width - 7,
                 font_name->name);
        waddstr(win, buf_str);
        mvwaddstr(win, FONT_NAME_OFFSET + 1, 0, font_name->name + width - 7);
    } else
        waddstr(win, font_name->name);
    wattroff(win, pair);
    if (fonts->names[fonts->cur_font_idx].len < width - 7) {
        wmove(win, FONT_NAME_OFFSET + 1, 0);
        wclrtoeol(win);
    }
    snprintf(buf_str, sizeof(buf_str), "Style:       ");
    mvwaddstr(win, FONT_STYLE_OFFSET, 0, buf_str);
    wclrtoeol(win);
    pair = (tui->cur_active_field == BB_ACTIVE_FONT_STYLE)
           ? tui->sel_pair : tui->unsel_pair;
    wattrset(win, pair);
    waddstr(win, fonts->style[fonts->cur_style_idx]);
    wattroff(win, pair);

    pair = (tui->cur_active_field == BB_ACTIVE_FONT_SZ)
           ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, FONT_SZ_OFFSET, 0, "Size:        ");
    wclrtoeol(win);
    wattrset(win, pair);
    snprintf(buf_str, sizeof(buf_str), "%d", fonts->cur_sz);
    waddstr(win, buf_str);
    wattroff(win, pair);

    pair = (tui->cur_active_field == BB_ACTIVE_ROTATION)
           ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, ROTATION_OFFSET, 0, "Rotation:    ");
    wclrtoeol(win);
    wattrset(win, pair);
    snprintf(buf_str, sizeof(buf_str), "%u°", conf->cur_rotation);
    waddstr(win, buf_str);
    wattroff(win, pair);

    pair = (tui->cur_active_field == BB_ACTIVE_ANTIALIAS)
           ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, ANTIALIAS_OFFSET, 0, "Antialias:   ");
    wclrtoeol(win);
    wattrset(win, pair);
    snprintf(buf_str, sizeof(buf_str), "%s",
             conf->cur_antialias ? "on" : "off");
    waddstr(win, buf_str);
    wattroff(win, pair);

    pair = (tui->cur_active_field == BB_ACTIVE_THRESHOLD)
           ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, THRESHOLD_OFFSET, 0, "Threshold:   ");
    wclrtoeol(win);
    wattrset(win, pair);
    snprintf(buf_str, sizeof(buf_str), "%d", conf->cur_threshold);
    waddstr(win, buf_str);
    wattroff(win, pair);

    pair = (tui->cur_active_field == BB_ACTIVE_SHIFT_DOWN)
           ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, SHIFT_DOWN_OFFSET, 0, "Shift Down:  ");
    wclrtoeol(win);
    wattrset(win, pair);
    snprintf(buf_str, sizeof(buf_str), "%d", conf->cur_shift_down);
    waddstr(win, buf_str);
    wattroff(win, pair);

    pair = (tui->cur_active_field == BB_ACTIVE_SHIFT_RIGHT)
           ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, SHIFT_RIGHT_OFFSET, 0, "Shift Right: ");
    wclrtoeol(win);
    wattrset(win, pair);
    snprintf(buf_str, sizeof(buf_str), "%d", conf->cur_shift_right);
    waddstr(win, buf_str);
    wattroff(win, pair);

    pair = (tui->cur_active_field == BB_ACTIVE_INVERSE)
           ? tui->sel_pair : tui->unsel_pair;
    mvwaddstr(win, INVERSE_OFFSET, 0, "Inverse:     ");
    wclrtoeol(win);
    wattrset(win, pair);
    waddstr(win, conf->cur_inverse ? "on" : "off");
    wattroff(win, pair);

    wmove(win, 0, width - 1);
    wattrset(win, tui->sep_pair);
    wvline(win, 0, height);
    wattroff(win, tui->sep_pair);

    return true;
} /* display_options */

static int qsort_strcmp(const void *d1, const void *d2);

static bool
load_font_maps(bb_tui_t *tui)
{
    bb_fonts_t *fonts = &tui->fonts;
    if (fonts->map) return true;

    fonts->map = pango_cairo_font_map_get_default();
    if (fonts->map == NULL) {
        tui->err_msg = "Failed to load default font map.";
        return false;
    }
    pango_font_map_list_families(fonts->map,
                                 &fonts->families,
                                 &fonts->n_families);
    if (fonts->n_families) {
        fonts->names = calloc(fonts->n_families, sizeof(bb_fn_name_t));
        if (fonts->names == NULL) {
            tui->err_msg = "failed to allocate memory for font names.";
            return false;
        }
        for (int i = 0; i < fonts->n_families; ++i) {
            const char *fn_name;
            fn_name = pango_font_family_get_name(fonts->families[i]);

            fonts->names[i].name = fn_name;
            fonts->names[i].len  = strlen(fn_name);
        }
        // do a quick sort of the names:
        qsort(fonts->names, fonts->n_families, sizeof(bb_fn_name_t),
              qsort_strcmp);
    } else {
        tui->err_msg = "No fonts could be found.";
        return false;
    }

    // default to use the first font
    tui->fonts.cur_font_idx = 0;
    tui->fonts.font_idx     = 0;
    return true;
} /* load_font_maps */

static void
tui_ctrl_option_update(bb_tui_t *tui, wint_t key)
{
    bool       need_update = false;
    bb_conf_t  *conf       = tui->conf;
    bb_fonts_t *fonts      = &tui->fonts;

    switch (key) {
    case KEY_UP:
    case KEY_LEFT:
        decrement_active_field_value(tui);
        need_update = true;
        break;
    case KEY_DOWN:
    case KEY_RIGHT:
        increment_active_field_value(tui);
        need_update = true;
        break;
    case KEY_NPAGE:
        if (tui->cur_active_field == BB_ACTIVE_FONT_NAME) {
            if (fonts->n_families > 20)
                fonts->cur_font_idx += 20;
            else
                fonts->cur_font_idx += fonts->n_families / 2;
            if (fonts->cur_font_idx >= fonts->n_families)
                fonts->cur_font_idx = fonts->cur_font_idx - fonts->n_families;
            need_update = true;
        }
        break;
    case KEY_PPAGE:
        if (tui->cur_active_field == BB_ACTIVE_FONT_NAME) {
            if (fonts->n_families > 20)
                fonts->cur_font_idx -= 20;
            else
                fonts->cur_font_idx -= fonts->n_families / 2;

            if (fonts->cur_font_idx < 0)
                fonts->cur_font_idx += fonts->n_families;
            need_update = true;
        }
        break;
    case ' ':
        if (tui->cur_active_field == BB_ACTIVE_INVERSE) {
            conf->cur_inverse = !conf->cur_inverse;
            need_update       = true;
        } else if (tui->cur_active_field == BB_ACTIVE_ANTIALIAS) {
            conf->cur_antialias = !conf->cur_antialias;
            need_update         = true;
        }
        break;
    default:
        break;
    } // switch
    display_status_win(tui);
    if (need_update)
        display_options(tui);
} /* tui_ctrl_option_update */

static bool
render_msg(bb_tui_t *tui)
{
    bool            ret    = false;
    bb_fonts_t      *fonts = &tui->fonts;
    const bb_conf_t *conf  = tui->conf;
    int             width, height;
    char            font_str[256];
    uint8_t         *out_dat = NULL;
    int             out_width, out_height;
    WINDOW          *win = panel_window(tui->panel[DISPLAY_WIN_ID]);
    char            *line;
    int             x, y;

    if (!tui->resize
        && tui->msg_len == tui->cur_msg_len
        && fonts->cur_font_idx == fonts->font_idx
        && fonts->cur_sz == fonts->sz
        && conf->cur_inverse == fonts->inverse
        && conf->cur_antialias == fonts->antialias
        && conf->cur_threshold == fonts->threshold
        && conf->cur_rotation == fonts->rotation
        && fonts->cur_style_idx == fonts->style_idx
        && tui->shift_down == conf->cur_shift_down
        && tui->shift_right == conf->cur_shift_right) {
        return true;
    }
    tui->resize = false;
    if (tui->cur_msg_len == 0) {
        wclear(win);
        show_logo(tui);
        ret = true;
        goto exit;
    }

    snprintf(font_str, sizeof(font_str), "%s %s %d",
             fonts->names[fonts->cur_font_idx].name,
             fonts->style[fonts->cur_style_idx], fonts->cur_sz);

    if (!bb_pango_render(fonts->map,
                         font_str,
                         tui->msg,
                         conf->cur_rotation,
                         conf->cur_threshold,
                         conf->cur_antialias,
                         conf->cur_inverse,
                         conf->cur_shift_down,
                         conf->cur_shift_right,
                         &tui->err_msg,
                         &out_dat,
                         &out_width,
                         &out_height))
        goto exit;

    wclear(win);
    if (out_dat == NULL) {
        ret = true;
        goto exit;
    }
    getmaxyx(win, height, width);

    x    = width > out_width ? (width - out_width) / 2 : 0;
    y    = height > out_height ? (height - out_height) / 2 : 0;
    line = (char *)out_dat;
    for (int i = y; i < out_height + y; ++i) {
        char *line_end = strchr(line, '\n');
        wmove(win, i, 0);
        wclrtoeol(win);
        if (line_end)
            *line_end = '\0';
        mvwaddstr(win, i, x, line);
        if (line_end)
            line = line_end + 1;
        else
            break;
    }
    free(out_dat);
    ret = true;
exit:
    tui->msg_len     = tui->cur_msg_len;
    tui->shift_down  = conf->cur_shift_down;
    tui->shift_right = conf->cur_shift_right;
    fonts->font_idx  = fonts->cur_font_idx;
    fonts->sz        = fonts->cur_sz;
    fonts->antialias = conf->cur_antialias;
    fonts->threshold = conf->cur_threshold;
    fonts->inverse   = conf->cur_inverse;
    fonts->rotation  = conf->cur_rotation;
    fonts->style_idx = fonts->cur_style_idx;
    return ret;
} /* render_msg */

static void
show_logo(bb_tui_t *tui)
{
    WINDOW *win = panel_window(tui->panel[DISPLAY_WIN_ID]);
    int    width, height;
    int    x, y;
    char   logo[]      = BB_LOGO;
    char   *line       = logo;
    int    ver_str_len = sizeof(BB_VER_COPYRIGHT_STR) - 1;
    int    i;

    getmaxyx(win, height, width);
    x = width > BB_LOGO_WIDTH ? (width - BB_LOGO_WIDTH) / 2 : 0;
    y = height > BB_LOGO_HEIGHT ? (height - BB_LOGO_HEIGHT) / 2 : 0;
    for (i = y; i < BB_LOGO_HEIGHT + y; ++i) {
        char *line_end = strchr(line, '\n');
        wmove(win, i, 0);
        wclrtoeol(win);
        if (line_end)
            *line_end = '\0';
        if (line[0])
            mvwaddstr(win, i, x, line);
        if (line_end)
            line = line_end + 1;
        else
            break;
    }
    x = width > ver_str_len ? (width - ver_str_len) / 2 : 0;
    wmove(win, i, 0);
    wclrtoeol(win);
    mvwaddstr(win, i, x, BB_VER_COPYRIGHT_STR);
} /* show_logo */

static int
qsort_strcmp(const void *d1, const void *d2)
{
    const bb_fn_name_t *s1 = d1;
    const bb_fn_name_t *s2 = d2;
    return strcmp(s1->name, s2->name);
} /* qsort_strcmp */

/* Return true for need_update
 *        false no update
 */
static void
increment_active_field_value(bb_tui_t *tui)
{
    bb_conf_t  *conf  = tui->conf;
    bb_fonts_t *fonts = &tui->fonts;

    switch (tui->cur_active_field) {
    case BB_ACTIVE_FONT_NAME:
        ++fonts->cur_font_idx;
        if (fonts->cur_font_idx == fonts->n_families)
            fonts->cur_font_idx = 0;
        break;
    case BB_ACTIVE_FONT_STYLE:
        ++fonts->cur_style_idx;
        if (fonts->cur_style_idx == NUM_OF_STYLES)
            fonts->cur_style_idx = 0;
        break;
    case BB_ACTIVE_FONT_SZ:
        ++fonts->cur_sz;
        break;
    case BB_ACTIVE_ROTATION:
        switch (conf->cur_rotation) {
        case 0: conf->cur_rotation   = 90; break;
        case 90: conf->cur_rotation  = 180; break;
        case 180: conf->cur_rotation = 270; break;
        case 270: conf->cur_rotation = 0; break;
        }     /* switch */
        break;
    case BB_ACTIVE_THRESHOLD:
        ++conf->cur_threshold;
        if (conf->cur_threshold == 0)
            conf->cur_threshold = 30;
        break;
    case BB_ACTIVE_ANTIALIAS:
        conf->cur_antialias = !conf->cur_antialias;
        break;
    case BB_ACTIVE_SHIFT_DOWN:
        if (conf->cur_shift_down < 4)
            ++conf->cur_shift_down;
        else
            conf->cur_shift_down = 0;
        break;
    case BB_ACTIVE_SHIFT_RIGHT:
        if (conf->cur_shift_right < 4)
            ++conf->cur_shift_right;
        else
            conf->cur_shift_right = 0;
        break;
    case BB_ACTIVE_INVERSE:
        conf->cur_inverse = !conf->cur_inverse;
        break;
    default:
        break;
    }     /* switch */
} /* increment_active_field_value */

static void
decrement_active_field_value(bb_tui_t *tui)
{
    bb_conf_t  *conf  = tui->conf;
    bb_fonts_t *fonts = &tui->fonts;

    switch (tui->cur_active_field) {
    case BB_ACTIVE_FONT_NAME:
        if (fonts->cur_font_idx > 0)
            --fonts->cur_font_idx;
        else
            fonts->cur_font_idx = fonts->n_families - 1;
        break;
    case BB_ACTIVE_FONT_STYLE:
        if (fonts->cur_style_idx > 0)
            --fonts->cur_style_idx;
        else
            fonts->cur_style_idx = NUM_OF_STYLES - 1;
        break;
    case BB_ACTIVE_FONT_SZ:
        if (fonts->cur_sz > 5)
            --fonts->cur_sz;
        break;
    case BB_ACTIVE_ROTATION:
        switch (conf->cur_rotation) {
        case 0: conf->cur_rotation   = 270; break;
        case 270: conf->cur_rotation = 180; break;
        case 180: conf->cur_rotation = 90; break;
        case 90: conf->cur_rotation  = 0; break;
        }     /* switch */
        break;
    case BB_ACTIVE_THRESHOLD:
        --conf->cur_threshold;
        if (conf->cur_threshold < BB_MIN_THRESHOLD)
            conf->cur_threshold = 255;
        break;
    case BB_ACTIVE_ANTIALIAS:
        conf->cur_antialias = !conf->cur_antialias;
        break;
    case BB_ACTIVE_SHIFT_DOWN:
        if (conf->cur_shift_down > 0)
            --conf->cur_shift_down;
        else
            conf->cur_shift_down = 4;
        break;
    case BB_ACTIVE_SHIFT_RIGHT:
        if (conf->cur_shift_right > 0)
            --conf->cur_shift_right;
        else
            conf->cur_shift_right = 4;
        break;
    case BB_ACTIVE_INVERSE:
        conf->cur_inverse = !conf->cur_inverse;
        break;
    default:
        break;
    } /* switch */
} /* decrement_active_field_value */
// vim: set ts=8 sts=4 sw=4 smarttab expandtab spell :
