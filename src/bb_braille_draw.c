/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bb_braille_draw.h"
#include "bb_conf.h"

static uint32_t calculate_one_block(const uint8_t *img,
                                    int           width,
                                    int           height,
                                    int           bpp,
                                    int           pitch,
                                    uint8_t       threshold,
                                    bool          inverse);
static uint8_t calculate_braille_idx(int position);
static void    print_braille_utf8(uint8_t *img, uint32_t braille_code);
static void    print_endl(uint8_t *img);
static bool    is_image_empty(const uint8_t *img, uint8_t threshold);


bool
bb_braille_draw(const uint8_t *img_dat,
                int           width,
                int           height,
                int           bpp,
                int           pitch,
                uint8_t       threshold,
                bool          inverse,
                uint8_t       **out_dat,
                int           *o_width,
                int           *o_height)
{
    bool     output_stdout = out_dat == NULL;
    int      out_width;
    int      out_height;
    uint8_t  *bb_img = NULL;
    uint32_t braille_code;
    int      bytes_pp = bpp / 8;

    out_width  = (width + 1) >> 1;
    out_height = (height + 3) >> 2;
    if (width < 2 || height < 4) {
        fprintf(stderr,
                "input image too small, at least have 4x2 pixels\n");
        return false;
    }

    if (!output_stdout) {
        bb_img = calloc((out_width * 3 + 1) * out_height + 1, 1);
        if (bb_img == NULL) {
            return false;
        }
        *out_dat  = bb_img;
        *o_width  = out_width;
        *o_height = out_height;
    }

    int row, col;
    for (row = 0; row < height; row += 4) {
        for (col = 0; col < width; col += 2) {
            int block_width  = 2;
            int block_height = 4;
            if (col + 1 == width)
                block_width = 1;
            if (row + 3 >= height)
                block_height = height - row;

            braille_code = calculate_one_block(img_dat
                                               + row * pitch + col * bytes_pp,
                                               block_width,
                                               block_height,
                                               bpp,
                                               pitch,
                                               threshold,
                                               inverse);
            print_braille_utf8(bb_img, braille_code);
            if (bb_img) bb_img += 3;
        }  // for col
        print_endl(bb_img);
        if (bb_img) bb_img += 1;
    }  // for row
    return true;
} /* bb_braille_draw */


static uint8_t
calculate_braille_idx(int position)
{
    switch (position) {
    case 0: return 1 << 0;
    case 1: return 1 << 1;
    case 2: return 1 << 2;
    case 3: return 1 << 6;
    case 4: return 1 << 3;
    case 5: return 1 << 4;
    case 6: return 1 << 5;
    case 7: return 1 << 7;
    default: return 0;
    } /* switch */
} /* calculate_braille_idx */

static void
print_braille_utf8(uint8_t *img, uint32_t braille_code)
{
    uint8_t braille_char[4] = {0};
    if (!img)
        img = braille_char;
    img[0] = 0xE0 | ((braille_code >> 12) & 0xF);
    img[1] = 0x80 | ((braille_code >> 6) & 0x3F);
    img[2] = 0x80 | (braille_code & 0x3F);

    if (img == braille_char)
        printf("%s", img);
} /* print_braille_utf8 */

static void
print_endl(uint8_t *img)
{
    if (img) {
        img[0] = '\n';
    } else {
        printf("\n");
    }
} /* print_endl */

static uint32_t
calculate_one_block(const uint8_t *img,
                    int           width,
                    int           height,
                    int           bpp,
                    int           pitch,
                    uint8_t       threshold,
                    bool          inverse)
{
    int            bytes_pp     = bpp / 8;
    uint8_t        braille_idx  = 0;
    const uint32_t braille_base = 0x2800;
    int            row, col;

    for (row = 0; row < height; ++row) {
        for (col = 0; col < width; ++col) {
            if (bpp == 32) {
                int idx = row * pitch + col * bytes_pp;
                if (!is_image_empty(img + idx, threshold)) {
                    braille_idx += calculate_braille_idx(row + (col << 2));
                }
            }
        }
    }

    if (inverse) braille_idx = 255 - braille_idx;
    return braille_base + braille_idx;
} /* calculate_one_block */

static bool
is_image_empty(const uint8_t *img, uint8_t threshold)
{
    if (img[0] >= threshold || img[1] >= threshold || img[2] >= threshold)
        return false;
    return true;
} /* is_image_empty */
/* vim: set ts=8 sts=3 sw=3 smarttab expandtab spell */
