/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <errno.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "bb_conf.h"
#include "bb_display_default_banner.h"
#include "bb_parse_argv.h"
#include "bb_tui.h"

int
main(int argc, char **argv)
{
    bb_conf_t conf = {
        .font          = "Times 37",
        .cur_rotation  = 90,
        .cur_threshold = 96,
        .cur_inverse   = false,
        .arg           = NULL,
    };

    setlocale(LC_ALL, "");
    bb_parse_argv(argc, argv, &conf);

    if (conf.show_logo) {
        puts(BB_LOGO);
        return 0;
    }
    if (conf.arg) {
        bb_display_default_banner(&conf, conf.arg);
    } else if (isatty(STDIN_FILENO)) {
        bb_tui(&conf);
    } else {
        char    *line = NULL;
        size_t  n     = 0;
        ssize_t len;

        len = getline(&line, &n, stdin);
        if (len == -1) {
            fprintf(
                stderr, "getline() failed: %d:%s\n", errno, strerror(errno));
            return EXIT_FAILURE;
        }
        if (line[len - 1] == '\n')
            line[len - 1] = '\0';
        bb_display_default_banner(&conf, line);
        free(line);
    }
    return 0;
} /* main */

/* vim: set ts=8 sts=3 sw=3 smarttab expandtab spell */
