/*
 * SPDX-FileCopyrightText:
 *   Copyright (C) 2024 Zhu, Qun-Ying <zhu.qunying*gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef BB_PARSE_ARGV_H
#define BB_PARSE_ARGV_H

void bb_parse_argv(int argc, char *argv[], bb_conf_t *conf);
#endif // ifndef BB_PARSE_ARGV_H
/* vim: set ts=8 sts=3 sw=3 smarttab expandtab spell */
