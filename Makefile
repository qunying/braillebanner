# just make the compile less key stroke during development
#

.SUFFIX:
.PHONEY: all cppcheck fmt setup clean clean-all

BUILD_DIR=build
MESON=meson
NINJA=ninja
NINJA_OPT=
SRC_DIR=src

all:
	@if [ ! -d $(BUILD_DIR) ]; then $(MAKE) setup; fi
	$(NINJA) $(NINJA_OPT) -C $(BUILD_DIR)

cppcheck:
	$(NINJA) -C $(BUILD_DIR) cppcheck

fmt:
	uncrustify -c scripts/uncrustify.cfg --replace --no-backup $(SRC_DIR)/*.{c,h}

setup:
	$(MESON) setup $(BUILD_DIR)

clean:
	$(NINJA) -C $(BUILD_DIR) clean
	
clean-all:
	@rm -rf $(BUILD_DIR)
